# MobileNavigationLibrary 
Library for light and easy-to-use (mobile) MVC-based architecture.

Copyright (c) 2018-2022 Vakoms LLC.

## Functionalities

## Dependencies

| Library / Framework         | Version    | License       | Resource                                                       |
| --------------------------- | ---------- | ------------- | -------------------------------------------------------------- |
| [Qt](https://www.qt.io)     | 5.12.10 / 6.4.2   | LGPL v3       | [Qt for Open Source](https://www.qt.io/download-open-source?hsCtaTracking=9f6a2170-a938-42df-a8e2-a9f0b1d6cdce%7C6cb0de4f-9bb5-4778-ab02-bfb62735f3e5) |

## License

**MobileNavigationLibrary** is licensed under the **GNU Lesser General Public License (LGPL) version 2.1**,
which means that you are free to get and use it for commercial and non-commercial purposes as long as
you fulfill its conditions.

See the [LICENSE](./LICENSE) file for more details.

## Building Instruction

## How to use
