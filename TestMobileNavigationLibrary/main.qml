/*
 * Mobile Navigation Library
 * The light and easy-to-use (mobile) MVC-based architecture
 * Copyright (C) 2021-2022 Vakoms LLC
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

import QtQuick.Controls 2.12
import QtQuick 2.12
import QtQml 2.12
import MobileNavigationLibrary 1.0

ApplicationWindow {
    width: 640
    height: 480
    visible: true
    title: qsTr("MobileNavigationLibrary Example Gallery")

    MenuDrawer {
        id: menu
        width: 0.8 * parent.width
        height: parent.height
        navigationController: AppNavigationController
    }

    ScreenView {
        id: view
        anchors.fill: parent
        navigationController: AppNavigationManager.navigationController("default")
    }
}
