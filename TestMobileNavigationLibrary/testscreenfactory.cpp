/*
 * Mobile Navigation Library
 * The light and easy-to-use (mobile) MVC-based architecture
 * Copyright (C) 2021-2022 Vakoms LLC
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "testscreenfactory.h"
#include "controllers/testsecondscreencontroller.h"
#include "controllers/teststartscreencontroller.h"
#include "controllers/testmenucontroller.h"
#include "basescreencontroller.h"
#include <QQmlEngine>

TestScreenFactory::TestScreenFactory()
    : AbstractScreenFactory()
{
}

std::unique_ptr<BaseScreenController> TestScreenFactory::makeScreen(int screenType,
                                                                    const QVariant &data)
{
    std::unique_ptr<BaseScreenController> newController = nullptr;
    switch (screenType)
    {
    case TestScreenTypes::MenuController:
        newController = std::make_unique<TestMenuController>();
        break;
    case TestScreenTypes::StartScreen:
        newController = std::make_unique<TestStartScreenController>();
        break;
    case TestScreenTypes::SecondScreen:
        newController = std::make_unique<TestSecondScreenController>();
        break;
    }
    if (newController)
    {
        newController->init(data);
        newController->setScreenType(screenType);
    }
    return newController;
}
