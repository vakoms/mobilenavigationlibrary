/*
 * Mobile Navigation Library
 * The light and easy-to-use (mobile) MVC-based architecture
 * Copyright (C) 2021-2022 Vakoms LLC
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "teststartscreencontroller.h"
#include "navigationcontroller.h"
#include "testscreenfactory.h"

TestStartScreenController::TestStartScreenController(QObject *parent)
    : BaseScreenController(parent)
{
    mIsBackAvailable = false;
}

TestStartScreenController::~TestStartScreenController()
{
}

QString TestStartScreenController::ui() const
{
    return QStringLiteral("qrc:/screens/TestScreen.qml");
}

QString TestStartScreenController::title() const
{
    return tr("Start Screen Example");
}

void TestStartScreenController::pushNextScreen() const
{
    navigationController().push(TestScreenTypes::SecondScreen);
}
