/*
 * Mobile Navigation Library
 * The light and easy-to-use (mobile) MVC-based architecture
 * Copyright (C) 2021-2022 Vakoms LLC
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "testmenucontroller.h"
#include "navigationcontroller.h"

TestMenuController::TestMenuController(QObject *parent)
    : BaseScreenController(parent)
{
    connect(this, &TestMenuController::navigationControllerChanged,
            this, &TestMenuController::connectNavigationController, Qt::UniqueConnection);
}

TestMenuController::~TestMenuController()
{
}

QString TestMenuController::ui() const
{
    return QStringLiteral("qrc:/screens/TestMenuScreen.qml");
}

QString TestMenuController::title() const
{
    return tr("Menu Example");
}

int TestMenuController::activeScreen() const
{
    return navigationController().currentScreenType();
}

void TestMenuController::connectNavigationController(NavigationController *controller)
{
    if (controller)
    {
        connect(controller, &NavigationController::currentScreenChanged,
                this, &TestMenuController::activeScreenChanged, Qt::UniqueConnection);
    }
}
