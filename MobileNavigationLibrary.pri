INCLUDEPATH += $$PWD/MobileNavigationLibrary $$PWD

android {
    equals(QT_VERSION, 5.15) | greaterThan(QT_VERSION, 5.15) {
        LIBS += -L$$DESTDIR -lMobileNavigationLibrary_$${ANDROID_TARGET_ARCH}
    } else {
        LIBS += -L$$DESTDIR -lMobileNavigationLibrary
    }
} else {
    LIBS += -L$$DESTDIR -lMobileNavigationLibrary
}

ios {
    RESOURCES += $$PWD/MobileNavigationLibrary/resources/mobileNavigationLibrary.qrc
}
