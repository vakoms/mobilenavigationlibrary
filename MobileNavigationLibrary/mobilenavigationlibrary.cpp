/*
 * Mobile Navigation Library
 * The light and easy-to-use (mobile) MVC-based architecture
 * Copyright (C) 2021-2022 Vakoms LLC
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "mobilenavigationlibrary.h"
#include "navigationcontroller.h"
#include "basescreencontroller.h"
#include "navigationmanager.h"
#include "controllertypes.h"
#include <QAbstractItemModel>
#include <QVersionNumber>
#include <QQmlEngine>

static const QVersionNumber scVersion(MAJOR_VERSION, MINOR_VERSION, BUILD_VERSION);
static const char *scQmlModuleName{TARGET};

QVersionNumber MobileNavigationLibrary::versionNumber()
{
    return scVersion;
}

QString MobileNavigationLibrary::version()
{
    return scVersion.toString();
}

void MobileNavigationLibrary::initialize(QQmlEngine &engine)
{
    // NOTE: Import path for QML Import Module that allows loading module in runtime.
    engine.addImportPath(QStringLiteral("qrc:/"));

#if QT_VERSION < QT_VERSION_CHECK(5, 14, 0)
    qmlRegisterType<QAbstractItemModel>();
#else
    qmlRegisterAnonymousType<QAbstractItemModel>("QAbstractItemModel", scVersion.majorVersion());
#endif
    qmlRegisterUncreatableType<BaseScreenController>(
        scQmlModuleName, scVersion.majorVersion(), scVersion.minorVersion(), "BaseScreenController",
        QStringLiteral("Could not instantiate BaseScreenController type from QML."));
    qmlRegisterUncreatableType<NavigationController>(
        scQmlModuleName, scVersion.majorVersion(), scVersion.minorVersion(), "NavigationController",
        QStringLiteral("Could not instantiate NavigationController type from QML."));
    qmlRegisterUncreatableType<NavigationManager>(
        scQmlModuleName, scVersion.majorVersion(), scVersion.minorVersion(), "NavigationManager",
        QStringLiteral("Could not instantiate NavigationManager type from QML."));
#if (QT_VERSION > QT_VERSION_CHECK(6, 0, 0))
    qmlRegisterUncreatableMetaObject(
        ControllerTypes::staticMetaObject, scQmlModuleName, scVersion.majorVersion(),
        scVersion.minorVersion(), "ControllerTypes",
        QStringLiteral("Could not instantiate ControllerTypes enum type."));
#else
    qmlRegisterUncreatableType<ControllerTypes>(
        scQmlModuleName, scVersion.majorVersion(), scVersion.minorVersion(), "ControllerTypes",
        QStringLiteral("Could not instantiate ControllerTypes enum type."));
#endif
}
