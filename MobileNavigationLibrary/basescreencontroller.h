/*
 * Mobile Navigation Library
 * The light and easy-to-use (mobile) MVC-based architecture
 * Copyright (C) 2018-2022 Vakoms LLC
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef BASESCREENCONTROLLER_H
#define BASESCREENCONTROLLER_H

#include "mobilenavigationlibrary_global.h"

#include <QObject>
#include <QVariant>

class NavigationController;

class MOBILENAVIGATIONLIBRARYSHARED_EXPORT BaseScreenController : public QObject
{
    Q_OBJECT
public:
    explicit BaseScreenController(QObject *parent = nullptr);
    ~BaseScreenController() override;

    Q_INVOKABLE virtual QString ui() const;

    Q_INVOKABLE QString uuid() const;

    void setScreenType(int screenType);
    Q_INVOKABLE int screenType() const;

    void setNavigationController(NavigationController *controller);
    NavigationController &navigationController() const;

    virtual void init(const QVariant &data);

    bool isBackAvailable() const;

public slots:
    virtual void pop(const QVariant &data = QVariant(), int transition = -1);

    virtual void onResultData(const QVariant &data);
    virtual void onScreenInactive();
    virtual void onScreenDeactivating();
    virtual void onScreenActivating();
    virtual void onScreenActive();
    virtual void onScreenRemoved();

    void readyToDestroy();

signals:
    void navigationControllerChanged(NavigationController *controller);

protected:
    bool mIsBackAvailable;

private:
    int mScreenType;
    QString mUuid;
    NavigationController *mNavigationController;
};

#endif // BASESCREENCONTROLLER_H
