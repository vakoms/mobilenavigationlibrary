/*
 * Mobile Navigation Library
 * The light and easy-to-use (mobile) MVC-based architecture
 * Copyright (C) 2018-2022 Vakoms LLC
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef ABSTRACTSCREENFACTORY_H
#define ABSTRACTSCREENFACTORY_H

#include "mobilenavigationlibrary_global.h"

#include <QVariant>
#include <memory>

class BaseScreenController;

class MOBILENAVIGATIONLIBRARYSHARED_EXPORT AbstractScreenFactory
{
public:
    AbstractScreenFactory();
    virtual ~AbstractScreenFactory();

    virtual std::unique_ptr<BaseScreenController> makeScreen(
        int screenType, const QVariant &data = QVariant()) = 0;

    virtual bool canMake(int screenType) const;

    virtual void initialize();
};

#endif // ABSTRACTSCREENFACTORY_H
