#-------------------------------------------------
#
# Project created by QtCreator 2018-03-20T11:27:53
#
#-------------------------------------------------

QT       += quick quickcontrols2
QT       -= gui

CONFIG += c++14

TARGET = MobileNavigationLibrary
TEMPLATE = lib

DEFINES += MOBILENAVIGATIONLIBRARY_LIBRARY

DEFINES += MAJOR_VERSION=1
DEFINES += MINOR_VERSION=0
DEFINES += BUILD_VERSION=0
DEFINES += TARGET=\\\"$${TARGET}\\\"

isEmpty(DESTDIR) {
    DESTDIR = ./../bin
}

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000 # disables all the APIs deprecated before Qt 6.0.0

include(windows/windows.pri)

SOURCES += \
        compositescreenfactory.cpp \
        mobilenavigationlibrary.cpp \
        navigationcontroller.cpp \
        basescreencontroller.cpp \
        abstractscreenfactory.cpp \
        navigationmanager.cpp

HEADERS += \
        compositecontrollertypes.h \
        compositescreenfactory.h \
        controllertypes.h \
        mobilenavigationlibrary.h \
        mobilenavigationlibrary_global.h \
        navigationcontroller.h \
        basescreencontroller.h \
        abstractscreenfactory.h \
        navigationmanager.h

RESOURCES += \
        resources/mobileNavigationLibrary.qrc

# NOTE: Import path for QML Import Module that allows loading module for Qt Creator.
QML_IMPORT_PATH += $$PWD/resources

win32 {
    public_headers.path = $$DESTDIR/$$TARGET
    public_headers.files = ./*.h
    pri_file.path = $$DESTDIR
    pri_file.files = ../*.pri
    INSTALLS += public_headers pri_file
}

unix {
    target.path = /usr/lib
    INSTALLS += target
}
