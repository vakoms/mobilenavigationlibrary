/*
 * Mobile Navigation Library
 * The light and easy-to-use (mobile) MVC-based architecture
 * Copyright (C) 2022 Vakoms LLC
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef NAVIGATIONMANAGER_H
#define NAVIGATIONMANAGER_H

#include "mobilenavigationlibrary_global.h"

#include <QObject>
#include <memory>
#include <map>

class NavigationController;

class MOBILENAVIGATIONLIBRARYSHARED_EXPORT NavigationManager : public QObject
{
    Q_OBJECT
public:
    explicit NavigationManager(QObject *parent = nullptr);
    ~NavigationManager() override;

    void addNavigationController(const QString &name,
                                 std::unique_ptr<NavigationController> controller);
    Q_INVOKABLE NavigationController *navigationController(const QString &name) const;

    void registerTypes() const;

private:
    std::map<QString, std::unique_ptr<NavigationController>> mControllers;
};

#endif // NAVIGATIONMANAGER_H
