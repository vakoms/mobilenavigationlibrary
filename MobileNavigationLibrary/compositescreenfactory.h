/*
 * Mobile Navigation Library
 * The light and easy-to-use (mobile) MVC-based architecture
 * Copyright (C) 2022 Vakoms LLC
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef COMPOSITESCREENFACTORY_H
#define COMPOSITESCREENFACTORY_H

#include "abstractscreenfactory.h"
#include "mobilenavigationlibrary_global.h"
#include <memory>
#include <vector>

class MOBILENAVIGATIONLIBRARYSHARED_EXPORT CompositeScreenFactory : public AbstractScreenFactory
{
public:
    CompositeScreenFactory() = default;
    CompositeScreenFactory(const CompositeScreenFactory &factory) = delete;
    CompositeScreenFactory(CompositeScreenFactory &&factory) = default;

    CompositeScreenFactory &operator=(const CompositeScreenFactory &factory) = delete;
    CompositeScreenFactory &operator=(CompositeScreenFactory &&factory) = default;

    std::unique_ptr<BaseScreenController> makeScreen(
        int screenType, const QVariant &data = QVariant()) override;

    bool canMake(int screenType) const override;

    void initialize() override;

    void addScreenFactory(std::unique_ptr<AbstractScreenFactory> screenFactory);

private:
    std::vector<std::unique_ptr<AbstractScreenFactory>> mFactories;
};

#endif // COMPOSITESCREENFACTORY_H
