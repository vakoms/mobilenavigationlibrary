/*
 * Mobile Navigation Library
 * The light and easy-to-use (mobile) MVC-based architecture
 * Copyright (C) 2018-2022 Vakoms LLC
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "navigationcontroller.h"
#include "mobilenavigationlibrary.h"
#include "abstractscreenfactory.h"
#include "basescreencontroller.h"
#include <QAbstractItemModel>
#include <vector>

struct DeleteLater
{
    void operator()(BaseScreenController *object)
    {
        object->deleteLater();
    }
};

using BaseScreenControllerPtr = std::unique_ptr<BaseScreenController, DeleteLater>;

struct NavigationController::NavigationControllerPrivate
{
    std::unique_ptr<AbstractScreenFactory> screenFactory;
    std::vector<BaseScreenControllerPtr> screenStack;
    QMap<int, BaseScreenController*> standardControllerMap;
    bool isDoubleClickBackAvailable = true;
    bool isBusy = false;
};

NavigationController::NavigationController(std::unique_ptr<AbstractScreenFactory> screenFactory,
                                           QObject *parent)
    : QObject(parent)
    , mData{std::make_unique<NavigationControllerPrivate>()}
{
    mData->screenFactory = std::move(screenFactory);
}

NavigationController::~NavigationController()
{
    qDeleteAll(mData->standardControllerMap);
}

void NavigationController::registerTypes() const
{
    mData->screenFactory->initialize();
}

void NavigationController::setBusy(bool busy)
{
    if (busy != mData->isBusy)
    {
        mData->isBusy = busy;
        emit busyChanged();
    }
}

bool NavigationController::isBusy() const
{
    return mData->isBusy;
}

int NavigationController::currentScreenType() const
{
    if (!mData->screenStack.empty())
    {
        return mData->screenStack.front()->screenType();
    }
    return -1;
}

BaseScreenController *NavigationController::standardController(int screen, const QVariant &data)
{
    auto screenController = mData->standardControllerMap.value(screen, nullptr);
    if (!screenController)
    {
        screenController = mData->screenFactory->makeScreen(screen, data).release();
        if (screenController)
        {
            screenController->setNavigationController(this);
            mData->standardControllerMap.insert(screen, screenController);
        }
    }
    return screenController;
}

bool NavigationController::isBackAvailable() const
{
    if (!mData->screenStack.empty())
    {
        return mData->screenStack.front()->isBackAvailable();
    }
    return false;
}

void NavigationController::setDoubleClickBackAvailability(bool isDoubleClickBackAvailable)
{
    if (mData->isDoubleClickBackAvailable != isDoubleClickBackAvailable)
    {
        mData->isDoubleClickBackAvailable = isDoubleClickBackAvailable;
        emit doubleClickBackAvailableChanged(mData->isDoubleClickBackAvailable);
    }
}

bool NavigationController::isDoubleClickBackAvailable() const
{
    return mData->isDoubleClickBackAvailable;
}

bool NavigationController::contains(int screen) const
{
    const auto it = std::find_if(mData->screenStack.cbegin(), mData->screenStack.cend(),
                                 [screen](const BaseScreenControllerPtr &controller) {
                                     return screen == controller->screenType();
                                 });
    return it != mData->screenStack.cend();
}

/*!
 * \brief Pushes screen with \a data by its \a screen index.
 * \a transition specifies the transition between current and pushed screen.
 * For more details, see QML StackView documentation.
 *
 * \param screen The index of the pushed screen.
 * \param data The data for the screen initialization.
 * \param transition The index of the screen transition.
 */
void NavigationController::push(int screen, const QVariant &data, int transition)
{
    if (!mData->screenStack.empty() && (mData->screenStack.front()->screenType() == screen))
    {
        return;
    }

    auto screenController = mData->screenFactory->makeScreen(screen, data);
    const auto it = mData->screenStack.cbegin();
    screenController->setNavigationController(this);
    const int screenType = screenController->screenType();
    emit pushController(screenController.get(), transition);
    mData->screenStack.insert(it, BaseScreenControllerPtr(screenController.release()));
    if (screenType >= 0)
    {
        emit currentScreenChanged(screenType);
    }
}

void NavigationController::pop(const QVariant &data, int transition)
{
    if (mData->screenStack.size() > 1)
    {
        popToScreen(mData->screenStack.at(1)->uuid(), data, transition);
    }
}

void NavigationController::popAll(const QVariant &data, int transition)
{
    if (!mData->screenStack.empty())
    {
        popToScreen(mData->screenStack.back()->uuid(), data, transition);
    }
}

void NavigationController::popToScreen(int screen, const QVariant &data, int transition)
{
    const auto it = std::find_if(mData->screenStack.cbegin(), mData->screenStack.cend(),
                                 [screen](const BaseScreenControllerPtr &controller) {
                                     return screen == controller->screenType();
                                 });
    if (it != mData->screenStack.cend())
    {
        if (data.isValid())
        {
            (*it)->onResultData(data);
        }
        emit popController((*it).get(), transition);
    }
}

void NavigationController::popToScreen(const QString &uuid, const QVariant &data, int transition)
{
    const auto it = std::find_if(mData->screenStack.cbegin(), mData->screenStack.cend(),
                                 [uuid](const BaseScreenControllerPtr &controller) {
                                     return uuid == controller->uuid();
                                 });
    if (it != mData->screenStack.cend())
    {
        auto &controller = (*it);
        if (data.isValid())
        {
            controller->onResultData(data);
        }
        emit popController(controller.get(), transition);
    }
}

void NavigationController::replace(const QString &uuid, int screen, const QVariant &data,
                                   int transition)
{
    const auto it = std::find_if(mData->screenStack.cbegin(), mData->screenStack.cend(),
                                 [uuid](const BaseScreenControllerPtr &controller) {
                                     return uuid == controller->uuid();
                                 });
    if (it != mData->screenStack.cend())
    {
        auto screenController = mData->screenFactory->makeScreen(screen, data);
        const auto cit = mData->screenStack.cbegin();
        screenController->setNavigationController(this);
        const int screenType = screenController->screenType();
        emit replaceController((*it).get(), screenController.get(), transition);
        mData->screenStack.insert(cit, BaseScreenControllerPtr(screenController.release()));
        if (screenType >= 0)
        {
            emit currentScreenChanged(screenType);
        }
    }
}

void NavigationController::replaceTop(int screen, const QVariant &data, int transition)
{
    if (!mData->screenStack.empty() && mData->screenStack.front()->screenType() != screen)
    {
        replace(mData->screenStack.front()->uuid(), screen, data, transition);
    }
}

void NavigationController::replaceAll(int screen, const QVariant &data, int transition)
{
    if (!mData->screenStack.empty())
    {
        replace(mData->screenStack.back()->uuid(), screen, data, transition);
    }
}

void NavigationController::popStandardController(int screen)
{
    if (mData->standardControllerMap.contains(screen))
    {
        mData->standardControllerMap.take(screen)->deleteLater();
    }
}

void NavigationController::readyToDestroy(BaseScreenController *controller)
{
    mData->screenStack.erase(std::remove_if(mData->screenStack.begin(), mData->screenStack.end(),
                                            [controller](const BaseScreenControllerPtr &uptr) {
                                                return controller == uptr.get();
                                            }), mData->screenStack.cend());
    emit currentScreenChanged(currentScreenType());
}
