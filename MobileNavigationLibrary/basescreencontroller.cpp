/*
 * Mobile Navigation Library
 * The light and easy-to-use (mobile) MVC-based architecture
 * Copyright (C) 2018-2022 Vakoms LLC
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "basescreencontroller.h"
#include "navigationcontroller.h"
#include <QQmlEngine>
#include <QUuid>

BaseScreenController::BaseScreenController(QObject *parent)
    : QObject{parent}
    , mIsBackAvailable{true}
    , mScreenType{-1}
    , mNavigationController{nullptr}
{
    QQmlEngine::setObjectOwnership(this, QQmlEngine::CppOwnership);
    mUuid = QUuid::createUuid().toString();
}

BaseScreenController::~BaseScreenController()
{
}

QString BaseScreenController::ui() const
{
    return QString();
}

QString BaseScreenController::uuid() const
{
    return mUuid;
}

void BaseScreenController::setScreenType(int screenType)
{
    if (mScreenType != screenType)
    {
        mScreenType = screenType;
    }
}

int BaseScreenController::screenType() const
{
    return mScreenType;
}

void BaseScreenController::setNavigationController(NavigationController *controller)
{
    if (mNavigationController != controller)
    {
        mNavigationController = controller;
        emit navigationControllerChanged(mNavigationController);
    }
}

NavigationController &BaseScreenController::navigationController() const
{
    Q_ASSERT(mNavigationController);
    return *mNavigationController;
}

void BaseScreenController::init(const QVariant &data)
{
    Q_UNUSED(data)
}

bool BaseScreenController::isBackAvailable() const
{
    return mIsBackAvailable;
}

void BaseScreenController::pop(const QVariant &data, int transition)
{
    navigationController().pop(data, transition);
}

void BaseScreenController::onResultData(const QVariant &data)
{
    Q_UNUSED(data)
}

void BaseScreenController::onScreenInactive()
{
}

void BaseScreenController::onScreenDeactivating()
{
}

void BaseScreenController::onScreenActivating()
{
}

void BaseScreenController::onScreenActive()
{
}

void BaseScreenController::onScreenRemoved()
{
}

void BaseScreenController::readyToDestroy()
{
    navigationController().readyToDestroy(this);
}
