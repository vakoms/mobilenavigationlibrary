/*
 * Mobile Navigation Library
 * The light and easy-to-use (mobile) MVC-based architecture
 * Copyright (C) 2018-2022 Vakoms LLC
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

import QtQuick.Controls 2.12
import QtQuick 2.12
import QtQml 2.12
import MobileNavigationLibrary 1.0

Page {
    id: rootPage

    default property alias content: container.data
    property BaseScreenController controller: null

    focus: true

    Component.onDestruction: controller.readyToDestroy()

    StackView.onRemoved: controller.onScreenRemoved()
    StackView.onActivated: controller.onScreenActive()
    StackView.onActivating: controller.onScreenActivating()
    StackView.onDeactivating: controller.onScreenDeactivating()
    StackView.onDeactivated: controller.onScreenInactive()

    Item {
        id: container
        anchors.fill: rootPage.visible ? parent : undefined
    }

    function handleBackEvent() {}
}
