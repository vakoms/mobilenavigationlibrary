/*
 * Mobile Navigation Library
 * The light and easy-to-use (mobile) MVC-based architecture
 * Copyright (C) 2019-2022 Vakoms LLC
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

import QtQuick.Controls 2.12
import QtQuick 2.12
import QtQml 2.12

Rectangle {
    id: aboutToQuitTip

    readonly property bool running: aboutToQuitTip.opacity !== 0.0
    property alias hidingDuration: opacityAnimator.duration
    property alias displayDuration: timer.interval
    property alias text: label.text
    property real padding: 20

    height: label.height + padding
    width: label.width + padding
    radius: height
    color: "#2B2A2A"
    opacity: 0.0
    z: 1

    onOpacityChanged: {
        if (opacity === 1.0) {
            timer.start()
        }
    }

    Label {
        id: label
        anchors.centerIn: parent
        color: "#FFFFFF"
        font.pixelSize: 15
        horizontalAlignment: Text.AlignHCenter
    }

    OpacityAnimator {
        id: opacityAnimator
        target: aboutToQuitTip
        from: 1.0
        to: 0.0
        duration: 1000
    }

    Timer {
        id: timer
        interval: 1500
        onTriggered: opacityAnimator.running = true
    }

    function start() {
        if (timer.running || opacityAnimator.running) {
            opacityAnimator.stop()
            timer.stop()
        }
        opacity = 1.0
    }

    function stop() {
        opacityAnimator.stop()
        timer.stop()
        opacity = 0.0
    }
}
