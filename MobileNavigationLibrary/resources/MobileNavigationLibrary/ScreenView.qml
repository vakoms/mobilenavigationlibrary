/*
 * Mobile Navigation Library
 * The light and easy-to-use (mobile) MVC-based architecture
 * Copyright (C) 2018-2022 Vakoms LLC
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

import QtQuick.Controls 2.12
import QtQuick 2.12
import QtQml 2.12
import MobileNavigationLibrary 1.0

StackView {
    id: stackView

    property NavigationController navigationController: null
    property int easingType: Easing.OutCubic
    property int animationDuration: 250
    property alias busyRectColor: busyRect.color
    property alias busyRectOpacity: busyRect.opacity
    property alias aboutToQuitTip: defaultAboutToQuitTip

    focus: true
    focusPolicy: Qt.StrongFocus
    popEnter: Transition {
        NumberAnimation {
            property: "x"
            from: -stackView.width
            to: 0
            duration: stackView.animationDuration
            easing.type: stackView.easingType
        }
    }
    popExit: Transition {
        NumberAnimation {
            property: "x"
            from: 0
            to: stackView.width
            duration: stackView.animationDuration
            easing.type: stackView.easingType
        }
    }
    pushEnter: Transition {
        NumberAnimation {
            property: "x"
            from: stackView.width
            to: 0
            duration: stackView.animationDuration
            easing.type: stackView.easingType
        }
    }
    pushExit: Transition {
        NumberAnimation {
            property: "x"
            from: 0
            to: -stackView.width
            duration: stackView.animationDuration
            easing.type: stackView.easingType
        }
    }
    replaceEnter: Transition {
        NumberAnimation {
            property: "x"
            from: stackView.width
            to: 0
            duration: stackView.animationDuration
            easing.type: stackView.easingType
        }
    }
    replaceExit: Transition {
        NumberAnimation {
            property: "x"
            from: 0
            to: -stackView.width
            duration: stackView.animationDuration
            easing.type: stackView.easingType
        }
    }

    QtObject {
        id: internal

        function pushController(controller, transition) {
            let args = [controller.ui(), {"controller": controller}, transition]
            push.apply(stackView, args)
        }

        function replaceController(targetController, replaceController, transition) {
            let targetItem = findItemByController(stackView, targetController)
            let args = [targetItem, replaceController.ui(), {"controller": replaceController},
                        transition]
            replace.apply(stackView, args)
        }

        function popController(controller, transition) {
            let args = [findItemByController(stackView, controller), transition]
            pop.apply(stackView, args)
        }

        function findItemByController(containter, controller) {
            return containter.find(function(item) {
                return item.controller === controller
            })
        }

        function changeBackShortcutStatus(status) {
            backShortcut.enabled = status
        }
    }

    Shortcut {
        id: backShortcut
        sequences: Qt.platform.os === "ios" || Qt.platform.os === "android"
                   ? ["Esc", "Back"] : ["Esc", "Backspace"]
        enabled: stackView.visible && stackView.navigationController.isDoubleClickBackAvailable()
        onActivated: {
            if (!busyRect.visible) {
                currentItem.handleBackEvent()
                handleBackEvent()
            }
        }
    }

    Rectangle {
        id: busyRect
        visible: stackView.navigationController.busy
        parent: Overlay.overlay
        anchors.fill: parent
        color: "black"
        opacity: 0.7

        BusyIndicator {
            id: indicator
            running: visible
            anchors.centerIn: parent
        }

        MouseArea {
            anchors.fill: parent
            enabled: parent.visible
        }
    }

    AboutToQuitTip {
        id: defaultAboutToQuitTip
        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            leftMargin: 70
            rightMargin: 70
            bottomMargin: 70
        }
        text: qsTranslate("MobileNavigationLibrary",
                          "Please, click again to close \nthe application.")
    }

    Component.onCompleted: {
        stackView.navigationController.pushController.connect(internal.pushController)
        stackView.navigationController.replaceController.connect(internal.replaceController)
        stackView.navigationController.popController.connect(internal.popController)
        stackView.navigationController.doubleClickBackAvailableChanged.connect(
                    internal.changeBackShortcutStatus)
    }

    function handleBackEvent() {
        if (stackView.navigationController.isDoubleClickBackAvailable()) {
            if (stackView.navigationController.isBackAvailable()) {
                stackView.navigationController.pop()
            } else {
                if (aboutToQuitTip.running) {
                    Qt.quit()
                }
                aboutToQuitTip.start()
            }
        }
    }
}
