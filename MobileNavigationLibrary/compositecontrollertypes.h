/*
 * Mobile Navigation Library
 * The light and easy-to-use (mobile) MVC-based architecture
 * Copyright (C) 2022 Vakoms LLC
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef COMPOSITECONTROLLERTYPES_H
#define COMPOSITECONTROLLERTYPES_H

#include "mobilenavigationlibrary_global.h"
#include "controllertypes.h"

static const int scControllerOffset{100};

class MOBILENAVIGATIONLIBRARYSHARED_EXPORT CompositeControllerTypes : public ControllerTypes
{
    Q_GADGET
public:
    enum CompositeTypes
    {
        UserFactory0Controller = UserController,
        UserFactory1Controller = UserFactory0Controller + scControllerOffset,
        UserFactory2Controller = UserFactory1Controller + scControllerOffset,
        UserFactory3Controller = UserFactory2Controller + scControllerOffset,
        UserFactory4Controller = UserFactory3Controller + scControllerOffset,
        UserFactory5Controller = UserFactory4Controller + scControllerOffset,
        UserFactory6Controller = UserFactory5Controller + scControllerOffset,
        UserFactory7Controller = UserFactory6Controller + scControllerOffset,
        UserFactory8Controller = UserFactory7Controller + scControllerOffset,
        UserFactory9Controller = UserFactory8Controller + scControllerOffset
    };
    Q_ENUM(CompositeTypes)
};

#endif // COMPOSITECONTROLLERTYPES_H
