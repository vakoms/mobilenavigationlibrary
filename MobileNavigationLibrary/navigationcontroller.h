/*
 * Mobile Navigation Library
 * The light and easy-to-use (mobile) MVC-based architecture
 * Copyright (C) 2018-2022 Vakoms LLC
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef NAVIGATIONCONTROLLER_H
#define NAVIGATIONCONTROLLER_H

#include "mobilenavigationlibrary_global.h"

#include <QVariant>
#include <QObject>
#include <memory>

class AbstractScreenFactory;
class BaseScreenController;

class MOBILENAVIGATIONLIBRARYSHARED_EXPORT NavigationController : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool busy READ isBusy WRITE setBusy NOTIFY busyChanged)
    Q_PROPERTY(int currentScreen READ currentScreenType NOTIFY currentScreenChanged)
public:
    explicit NavigationController(std::unique_ptr<AbstractScreenFactory> screenFactory,
                                  QObject *parent = nullptr);
    ~NavigationController();

    void registerTypes() const;

    void setBusy(bool busy);
    bool isBusy() const;

    int currentScreenType() const;

    Q_INVOKABLE BaseScreenController *standardController(int screen,
                                                         const QVariant &data = QVariant());

    Q_INVOKABLE bool isBackAvailable() const;

    Q_INVOKABLE void setDoubleClickBackAvailability(bool isDoubleClickBackAvailable);
    Q_INVOKABLE bool isDoubleClickBackAvailable() const;

    Q_INVOKABLE bool contains(int screen) const;

signals:
    void busyChanged();
    void doubleClickBackAvailableChanged(bool status);

    void currentScreenChanged(int screenType);
    void pushController(QObject *controller, int transition = -1);
    void replaceController(QObject *target, QObject *controller, int transition = -1);
    void popController(QObject *controller, int transition = -1);

public slots:
    void push(int screen, const QVariant &data = QVariant(), int transition = -1);
    void pop(const QVariant &data = QVariant(), int transition = -1);
    void popAll(const QVariant &data = QVariant(), int transition = -1);
    void popToScreen(int screen, const QVariant &data = QVariant(), int transition = -1);
    void popToScreen(const QString &uuid, const QVariant &data = QVariant(), int transition = -1);
    void replace(const QString &uuid, int screen, const QVariant &data = QVariant(),
                 int transition = -1);
    void replaceTop(int screen, const QVariant &data = QVariant(), int transition = -1);
    void replaceAll(int screen, const QVariant &data = QVariant(), int transition = -1);

    void popStandardController(int screen);

    void readyToDestroy(BaseScreenController *controller);

private:
    struct NavigationControllerPrivate;
    std::unique_ptr<NavigationControllerPrivate> mData;
};

#endif // NAVIGATIONCONTROLLER_H
