/*
 * Mobile Navigation Library
 * The light and easy-to-use (mobile) MVC-based architecture
 * Copyright (C) 2022 Vakoms LLC
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "compositescreenfactory.h"
#include "basescreencontroller.h"

std::unique_ptr<BaseScreenController> CompositeScreenFactory::makeScreen(
    int screenType, const QVariant &data)
{
    std::unique_ptr<BaseScreenController> newController;
    for (auto it = mFactories.cbegin(); it != mFactories.cend(); ++it)
    {
        if ((*it)->canMake(screenType))
        {
            newController = (*it)->makeScreen(screenType, data);
            break;
        }
    }
    return newController;
}

bool CompositeScreenFactory::canMake(int screenType) const
{
    const auto it = std::find_if(mFactories.cbegin(), mFactories.cend(),
                                 [&screenType](const std::unique_ptr<AbstractScreenFactory> &factory) {
                                     return factory->canMake(screenType);
                                 });
    return it != mFactories.cend();
}

void CompositeScreenFactory::initialize()
{
    for (auto it = mFactories.cbegin(); it != mFactories.cend(); ++it)
    {
        (*it)->initialize();
    }
}

void CompositeScreenFactory::addScreenFactory(std::unique_ptr<AbstractScreenFactory> screenFactory)
{
    if (screenFactory)
    {
        mFactories.push_back(std::move(screenFactory));
    }
}
